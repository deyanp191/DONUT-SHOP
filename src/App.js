import "./App.css";
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Start from "./components/Start/Start";
import Menu from "./components/Menu/Menu";
import Payment from "./components/Payment/Payment";
import Preparation from "./components/Preparation/Preparation";
import Ready from "./components/Ready/Ready";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>

          <Route exact path='/' element={<Start />} />
          <Route exact path='/menu' element={<Menu isExact/>} />
          <Route  path='/payment/:id' element={<Payment  />} />
          <Route exact path='/preparation/:id' element={<Preparation />} />
          <Route exact path='/ready/:id' element={<Ready />} />

        </Routes>
      </BrowserRouter>
    </div> 
  );
}

export default App;
