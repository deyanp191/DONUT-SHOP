import Donut from "../Donut/Donut";
import Title from "../Title/Title";
import styles from './Menu.module.css';

function Menu(props) {
    const products=[
        {id:1,name:'sky_shaped', picture:'/sky_shaped.svg'},
        {id:2,name:'marble_magic', picture:'/marble_magic.svg'},
        {id:3,name:'true_blood', picture:'/true_blood.svg'},
        {id:4,name:'unicorn_dust', picture:'/unicorn_dust.svg'}]
    return (
        <div className={styles.menu}>
            <Title text96='menu' text48='the' />
            <Donut className={styles.donuts} products={products} />
        </div>
    );
}

export default Menu;