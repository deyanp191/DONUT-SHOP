
import Title from "../Title/Title";
import styles from './Payment.module.css';
import { Link, useParams } from 'react-router-dom';

function Payment() {
  const {id}=useParams()
    console.log(id);
    return (
        <div className={styles.payment}>
            <Title text64='You can' text48='pay us' text96='now' ></Title>
         <Link to={`/preparation/${id}`} >  <div id="apple_pay"><img src='/apple-pay.png' /></div> </Link>
         <Link to={`/preparation/${id}`}  ><div id="google_pay"><img src='/google-pay.png' /></div></Link>
        </div> 
    );
}

export default Payment;