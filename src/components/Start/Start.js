import Title from '../Title/Title';
import {Link} from 'react-router-dom';
import styles from './Start.module.css';
import {AnimatePresence,motion} from "framer-motion/dist/framer-motion";

function Start(params) {
    return (
        <div className={styles.startPage}>
            <div className={styles.triangle}></div>
            <img className={styles.sparkles}  src="/sparkles.svg" alt="uunicorn" />
            <img className={styles.whiteDont}  src="/unicorn_dust.svg" alt="uunicorn" />
            <img className={styles.pinkDonut} src="/true_blood.svg" alt="true blood" />
            <Title text96='donut shop' text48='the' />
            
        <div className={styles.barBtn}>

         <Link className={styles.startBtn} to='/menu'>Start</Link>
        </div>
            
        </div>
    );
}
export default Start;