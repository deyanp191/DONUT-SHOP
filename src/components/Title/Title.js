
function Title(props) {
    
    return (
        <div className="title">
  
           <h2> { props.text48 } </h2>          
           <h1>{ props.text96 }</h1> 
           <p>{props.text40}</p>
        </div>
    );
}

export default Title;