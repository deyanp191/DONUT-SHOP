import {Link} from 'react-router-dom';
import {AnimatePresence,motion} from "framer-motion/dist/framer-motion";

function Donut({products}) {

  
    return (
        <div className='menuItems'>
             
          {products.length===4?products.map((x,i)=><Link key={x.id} data-id={x.id} to={`/payment${x.picture}`}><p className={`donut_name_${x.name}`}>{x.name.split('_').join(' ')}</p><div className={`donuts img_${x.name}`}><motion.img animate={{rotate:360}} transition={{ duration: 2 }}  src={x.picture} alt={x.name} /></div></Link> ) : (<div  className={`donut  img_${products}` }><motion.img animate={{rotate:360}} transition={{ duration: 5 }} src={`/${products}`} alt={products} /></div>)}
        
        </div>
    )
}
export default Donut;